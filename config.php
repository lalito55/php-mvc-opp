<?php 

define('URL', 'https://localhost/proyecto1');
define('LIBS', 'libs/');
define('BS', './bussinesLogic');
define('MODELS', './models');
define('MODULE', '/views/modules/');

define('DB_TYPE', `mysql`);
define('DB_HOST', `localhost`);
define('DB_USER', `root`);
define('DB_PASS', ``);
define('DB_NAME', `proyecto1`);

define('HASH_ALGO', 'sha512');
define('HASH_KEY', 'my_key');
define('HASH_SECRET', 'my_secret');
define('SECRET_WORD', 'so_secret');